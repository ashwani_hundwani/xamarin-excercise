﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.App;
using Exercise.ViewModels;
using Exercise.Models;
using System.Threading.Tasks;
using Exercise.Droid.Adapters;

namespace Exercise.Droid
{
    [Activity(Label = "ActorsActivity")]
    public class ActorsActivity : Activity
    {
        public ActorsViewModel ActorsViewModel;
        private ProgressDialog ProgressDialog;

        private void HideProgressDialog()
        {

            ProgressDialog.Hide();
        }

        private void ShowProgressDialog() {
            if(ProgressDialog == null) {

                ProgressDialog = new ProgressDialog(this);
                ProgressDialog.SetMessage("Downloading list of actors...");
                ProgressDialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            }

            ProgressDialog.Show();

        }


        private async Task FetchActorsListAsync()
        {
            ActorsViewModel = new ActorsViewModel();

            Actors actors = await ActorsViewModel.GetActorsAsync();

            ListView actorsListView = FindViewById<ListView>(Resource.Id.actors_list_view);

            actorsListView.Adapter = new ActorsAdapter(this);

            HideProgressDialog();
        }
          
        protected override void OnCreate(Android.OS.Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Actors);

            ShowProgressDialog();
            Task task = FetchActorsListAsync();
        }
    }
}
