﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Exercise.Droid.Utilities;
using Android.Net;
using Java.IO;

namespace Exercise.Droid
{
    
    [Activity(Label = "FullScreenImageActivity")]
    public class FullScreenImageActivity : Activity
    {
		private void AddClickListenerToCloseButton()
		{

            Button CloseButton = FindViewById<Button>(Resource.Id.btn_close);

			CloseButton.Click += (sender, e) =>
			{
                Finish();
			};

		}

        private void SetFullScreenProfilePic() {

            ImageView FullScreenImageView = FindViewById<ImageView>(Resource.Id.profile_image);
            String ImageURIString =  Intent.Extras.GetString(Constants.IntentDataKeys.ProfilePicURI);

            if(ImageURIString != null) {
                FullScreenImageView.SetImageURI(Android.Net.Uri.Parse(ImageURIString));
            }
            else {
				int height = Resources.DisplayMetrics.HeightPixels;
				int width = FullScreenImageView.Height;
				FullScreenImageView.SetImageBitmap(App._file.Path.LoadAndResizeBitmap(width, height));


			}
			
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

			//Remove title bar
            RequestWindowFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.FullScreenImage);
			AddClickListenerToCloseButton();
            SetFullScreenProfilePic();

			// Create your application here
		}
    }
}
