﻿﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Content;
using Exercise.Droid.Utilities;
using Android.Net;
using Java.IO;
using Android.Provider;
using System;
using Android.Graphics;
using System.Net.Mail;

namespace Exercise.Droid
{
    enum ValidationResult {

        Success,
        EmailIsEmpty,
        EmailInvalid,
        LanguageNotSelected,
    }
    
	enum RendererType
	{
		BitMap,
		Uri
	}

    [Activity(Label = "Exercise", MainLauncher = true, Icon = "@mipmap/icon")]

    public class RegistrationActivity : Activity
    {
        private Android.Net.Uri SelectedImageURI = null;
        private RendererType ImageRendererType;

        private Boolean IsLangugeSelected() {
            Boolean selected = false;

            CheckBox checkBoxEnglish = FindViewById<CheckBox>(Resource.Id.language_english);

            if(checkBoxEnglish.Checked) {
                selected = true;
                return selected;
            }

            CheckBox checkBoxChinese = FindViewById<CheckBox>(Resource.Id.language_chinese);

			if (checkBoxChinese.Checked)
			{
                selected = true;
				return selected;
			}

            CheckBox checkBoxRussian = FindViewById<CheckBox>(Resource.Id.language_russian);

			if (checkBoxRussian.Checked)
			{
                selected = true;
				return selected;
			}


            return selected;
        }
        private Boolean ValidateEmail() {

			EditText email = FindViewById<EditText>(Resource.Id.signup_input_email);
            Boolean isValidEmail = false;
			try
			{
				MailAddress m = new MailAddress(email.Text);

				isValidEmail = true;
			}
			catch (FormatException)
			{

			}

            return isValidEmail;
        }

        private ValidationResult ValidateForm() {
            ValidationResult result = ValidationResult.Success;
            EditText email = FindViewById<EditText>(Resource.Id.signup_input_email);
            if(email.Text.Length == 0) {
                result = ValidationResult.EmailIsEmpty;
                return result;
            }

            Boolean isEmailValid = ValidateEmail();

            if(!isEmailValid) {

				result = ValidationResult.EmailInvalid;
				return result;
            }

            Boolean languageSelected = IsLangugeSelected();

            if(!languageSelected) {
                result = ValidationResult.LanguageNotSelected;
            }

            return result;

        }

        private string MessageForValidationResult(ValidationResult result) {

            string message = null;
            switch (result)
            {

                case ValidationResult.EmailIsEmpty:
                    {
                        message = "Email cannot be blank";
                    }
                    break;

                case ValidationResult.EmailInvalid:
                    {
                        message = "Email is not valid";
                    }
                    break;
                case ValidationResult.LanguageNotSelected:
                    {
                        message = "Please select a language";
                    }
                    break;
            }

            return message;
        }

		private void CreateDirectoryForPictures()
		{
			App._dir = new File(
				Android.OS.Environment.GetExternalStoragePublicDirectory(
					Android.OS.Environment.DirectoryPictures), "Exercise");
			if (!App._dir.Exists())
			{
				App._dir.Mkdirs();
			}
		}

        private void OpenCamera() {

            ImageRendererType = RendererType.BitMap;
            CreateDirectoryForPictures();

			Intent intent = new Intent(MediaStore.ActionImageCapture);
			App._file = new File(App._dir, "myPhoto.jpg");
            intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(App._file));
			StartActivityForResult(intent, Constants.RequestCode.ProfileImageFromCamera);
        }

        private void OpenGallery()
        {
            ImageRendererType = RendererType.Uri;
			Intent intent = new Intent();
			intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"),Constants.RequestCode.ProfileImageFromGallery);
        }        

        private void ShowFullScreenImage() {

            Intent fullScreenActivityIntent = new Intent(this, typeof(FullScreenImageActivity));

            if(ImageRendererType == RendererType.Uri) {

                fullScreenActivityIntent.PutExtra(Constants.IntentDataKeys.ProfilePicURI, SelectedImageURI.ToString());
            }
            else {
                fullScreenActivityIntent.PutExtra(Constants.IntentDataKeys.ProfilePicBitmap, SelectedImageURI.ToString());
            }
			StartActivity(fullScreenActivityIntent);
        }

        private void SelectImage()
        {
            List<string> dialogOptions = new List<string>();

            dialogOptions.Add("Take Photo");
			dialogOptions.Add("Choose from Library");
			dialogOptions.Add("Cancel");


			AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.SetTitle("Add a profile picture");
            builder.SetItems(dialogOptions.ToArray(),(sender, e) => {
                switch(e.Which) {

                    // Take Photo
                    case 0: {
                            OpenCamera();
                        }
                        break;
                    // Choose from Library
                    case 1: {
                            OpenGallery();
                        }
                        break;

                    //Cancel
					case 2:{

						}
						break;

                }
            });

            builder.Show();
        }
        
        private void AddClickListenerToImageView() {

            ImageView profileImageView = FindViewById<ImageView>(Resource.Id.profile_image);

            profileImageView.Click += (sender, e) => {
                if(SelectedImageURI == null) {
                    SelectImage();
                }
                else {
                    ShowFullScreenImage();
                }
            };

        }

		private void AddClickListenerToRegisterButton()
		{

            Button registerButton = FindViewById<Button>(Resource.Id.btn_signup);

			registerButton.Click += (sender, e) =>
			{
                ValidationResult result = ValidateForm();

                if(result == ValidationResult.Success) {
					Intent actorsActivity = new Intent(this, typeof(ActorsActivity));
					StartActivity(actorsActivity);
                }
                else {
                    string message = MessageForValidationResult(result);

                    Toast.MakeText(this, message, ToastLength.Long).Show();
                }
				
			};

		}

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch(requestCode) {

                case Constants.RequestCode.ProfileImageFromCamera: {
						if (resultCode == Result.Ok)
						{
							if (resultCode == Result.Ok)
							{
								Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
								Android.Net.Uri contentUri = Android.Net.Uri.FromFile(App._file);
								mediaScanIntent.SetData(contentUri);
								SendBroadcast(mediaScanIntent);

								var imageView =
									FindViewById<ImageView>(Resource.Id.profile_image);
								
                                SelectedImageURI = contentUri;

								int height = Resources.DisplayMetrics.HeightPixels;
                                int width = imageView.Height;

                                imageView.SetImageBitmap(App._file.Path.LoadAndResizeBitmap(width, height));
								// Dispose of the Java side bitmap.
								GC.Collect();
							}
						}
                    }
                    break;
				case Constants.RequestCode.ProfileImageFromGallery: {
						if (resultCode == Result.Ok)
						{
							if (data == null)
							{
								//Display an error
								return;
							}
							if (resultCode == Result.Ok)
							{ 

								var imageView =
									FindViewById<ImageView>(Resource.Id.profile_image);
								imageView.SetImageURI(data.Data);
								SelectedImageURI = data.Data;
							}
						}
					}
					break;
            }


        }
		protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Registration);

            AddClickListenerToRegisterButton();

            AddClickListenerToImageView();

        }
    }
}

