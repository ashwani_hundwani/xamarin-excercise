﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Provider;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Exercise.ViewModels;
using Exercise.Models;
using Exercise.Droid;
using Square.Picasso;

namespace Exercise.Droid.Adapters
{
    public class ActorsAdapter : BaseAdapter
    {
        private List<Actor> ActorsList = null;
        private ActorsActivity ActorsActivity;
        public ActorsAdapter(ActorsActivity activity)
        {
            ActorsActivity = activity;
            ActorsList = activity.ActorsViewModel.Actors.actors;
        }

        public override int Count {

            get {

                return ActorsList.Count;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            throw new NotImplementedException();
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? ActorsActivity.LayoutInflater.Inflate(
                Resource.Layout.ActorListItem, parent, false);

            var actorNameTextView = view.FindViewById<TextView>(Resource.Id.actor_name);
            actorNameTextView.Text = ActorsList[position].Name;

            var countryNameTextView = view.FindViewById<TextView>(Resource.Id.actor_country);
            countryNameTextView.Text = ActorsList[position].Country;

            var ProfileImageView = view.FindViewById<ImageView>(Resource.Id.actor_image);

            // Download image from the URL & display it in image view.
            Picasso.With(ActorsActivity.BaseContext).Load(ActorsList[position].Image.ToString()).Into(ProfileImageView);

			return view;

		}
    }
}
