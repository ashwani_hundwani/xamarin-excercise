﻿using System;
using Android.Graphics;
using Android.Media;
using Android.Hardware.Camera2;

namespace Exercise.Droid.Utilities
{
	public static class BitmapHelpers
	{
        public static Bitmap LoadAndResizeBitmap(this string fileName, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                                   ? outHeight / height
                                   : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

            ExifInterface exifInterface = new ExifInterface(fileName);
            string tag =  exifInterface.GetAttribute(ExifInterface.TagOrientation);

            int rotate = 0;
            switch (Int16.Parse(tag)) {
                    //270
                case 8:
                    rotate = 270;
                    break;
                    //90 degree
				case 6:
					rotate = 90;
					break;
                    // 180 degree
				case 3:
					rotate = 180;
					break;
            }


            return RotateImage(resizedBitmap, rotate);
        }

		public static Bitmap RotateImage(Bitmap source, float angle)
		{
            
            //return source;
			Matrix matrix = new Matrix();
			matrix.PostRotate(angle);
            return Bitmap.CreateBitmap(source, 0, 0, source.Width, source.Height,
									   matrix, true);
		}
	}
}
