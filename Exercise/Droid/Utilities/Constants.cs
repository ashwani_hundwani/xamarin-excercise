﻿using System;
namespace Exercise.Droid.Utilities
{
    public static class Constants
    {
        public struct RequestCode {
            public const UInt16 ProfileImageFromGallery = 300;
			public const UInt16 ProfileImageFromCamera = 301;
		}

        public struct IntentDataKeys {

            public const String ProfilePicURI = "profile_pic_uri";
			public const String ProfilePicBitmap = "profile_pic_bitmap";
            public const String ProfilePicRendererType = "profile_pic_renderer_type";

		}

    }
}
