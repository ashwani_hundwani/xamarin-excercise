﻿using System;
using Android.Content.Res;
using Android.Graphics;

namespace Exercise.Droid.Utilities
{
    public static class Utils
    {
        public static Bitmap BitMapForFileAtUri(float width, float height){

			Bitmap = App._file.Path.LoadAndResizeBitmap(width, height);

        }
    }
}
