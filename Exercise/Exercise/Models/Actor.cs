﻿using System;
namespace Exercise.Models
{
    public class Actor
    {
        public string Name { get;  set; }
        public string Image { get;  set; }
		public string Country { get;  set; }


        public Actor(string name, string profilePicURL, string country)
        {
            Name = name;
            Image = profilePicURL;
            Country = country;
        }
    }
}
