﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Exercise.Models;
using Newtonsoft.Json;

namespace Exercise.Services
{
    public static class HttpService
    {
        static HttpClient client = new HttpClient();

        public static async Task<Actors> GetActorsAsync(Uri path)
		{
            Actors list  = null;
			HttpResponseMessage response = await client.GetAsync(path);
			if (response.IsSuccessStatusCode)
			{
                Task<string> json = response.Content.ReadAsStringAsync();
                string result = json.Result;

                list = JsonConvert.DeserializeObject<Actors>(result);

			}
			return list;
		}

	}
}
