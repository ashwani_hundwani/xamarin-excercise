﻿using System;
using Exercise.Services;
using Exercise.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exercise.ViewModels
{
    public class ActorsViewModel
    {
        public Actors Actors { get; private set; }

        public async Task<Actors> GetActorsAsync() {
            
           Actors listActors = await HttpService.GetActorsAsync(new Uri("http://microblogging.wingnity.com/JSONParsingTutorial/jsonActors"));

            Actors = listActors;
            return Actors;
        }
        public ActorsViewModel()
        {
            
		}
    }
}
